package com.barry.billingservice.dto.response;

import com.barry.billingservice.model.Customer;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class InvoiceResponse {

    @Id
    private String invoiceId;
    private LocalDate date;
    private BigDecimal amount;
    private String customerId;
    private Customer customer;
}
