package com.barry.billingservice.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class InvoiceRequest {
    private BigDecimal amount;
    private String customerId;
}
