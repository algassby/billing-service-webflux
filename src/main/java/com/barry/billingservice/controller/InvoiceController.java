package com.barry.billingservice.controller;

import com.barry.billingservice.dto.request.InvoiceRequest;
import com.barry.billingservice.dto.response.InvoiceResponse;
import com.barry.billingservice.service.InvoiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/invoices")
@RequiredArgsConstructor
public class InvoiceController {

    private final InvoiceService invoiceService;

    @GetMapping
    public Flux<InvoiceResponse> findAll(){
        return invoiceService.findAll();
    }

    @GetMapping("/{customerId}")
    public Flux<InvoiceResponse> findAllByCustomerId(@PathVariable String customerId){
        return invoiceService.findAllByCustomerId(customerId);
    }

    @PostMapping("/save")
    public Mono<InvoiceResponse> save(@RequestBody InvoiceRequest invoiceRequest){
        return invoiceService.save(invoiceRequest);
    }

}
