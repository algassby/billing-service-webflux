package com.barry.billingservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDate;

@Document("invoices")
@Data
@NoArgsConstructor
public class Invoice {

	@Id
	private String invoiceId;
	private LocalDate date;
	private BigDecimal amount;
	private String customerId;
	@Transient
	private Customer customer;
	
	
	
}