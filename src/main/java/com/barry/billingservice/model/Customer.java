package com.barry.billingservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Customer {

    private String customerId;
    private String fullName;
    private String email;
    private Long age;
    private String address;
    private String phoneNumber;
}
