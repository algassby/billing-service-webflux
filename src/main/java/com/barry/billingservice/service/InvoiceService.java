package com.barry.billingservice.service;

import com.barry.billingservice.dto.request.InvoiceRequest;
import com.barry.billingservice.dto.response.InvoiceResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface InvoiceService {

    Flux<InvoiceResponse> findAll();
    Flux<InvoiceResponse> findAllByCustomerId(String customerId);
    Mono<InvoiceResponse> findById(String invoiceId);
    Mono<InvoiceResponse> save(InvoiceRequest invoiceRequest);
    Mono<InvoiceResponse> update(InvoiceRequest invoiceRequest, String invoiceId);
    Mono<Void> delete(String invoiceId);
}
