package com.barry.billingservice.service;

import com.barry.billingservice.dto.request.InvoiceRequest;
import com.barry.billingservice.dto.response.InvoiceResponse;
import com.barry.billingservice.model.Customer;
import com.barry.billingservice.model.Invoice;
import com.barry.billingservice.repository.InvoiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

import static com.barry.billingservice.mapper.InvoiceMapper.INVOICE_MAPPER;

@Service
@RequiredArgsConstructor
public class InvoiceServiceImpl implements InvoiceService {

    private final InvoiceRepository invoiceRepository;
    private final CustomerService customerService;

    @Override
    public Flux<InvoiceResponse> findAll() {
        return invoiceRepository.findAll()
                .map(INVOICE_MAPPER::invoiceToInvoiceResponse);
    }

    @Override
    public Flux<InvoiceResponse> findAllByCustomerId(String customerId) {

        return invoiceRepository.findAllByCustomerId(customerId)

                .flatMap(invoice -> {
                    Mono<Customer> customerMono = customerService.getCustomer(customerId);

                  return   customerMono.flatMap(customer -> {
                        invoice.setCustomer(customer);
                        return Mono.just(invoice);
                    });

                })
                .map(INVOICE_MAPPER::invoiceToInvoiceResponse);

    }

    @Override
    public Mono<InvoiceResponse> findById(String invoiceId) {
        return invoiceRepository.findById(invoiceId)
                .map(INVOICE_MAPPER::invoiceToInvoiceResponse)
                .switchIfEmpty(Mono.empty());
    }

    @Override
    public Mono<InvoiceResponse> save(InvoiceRequest invoiceRequest) {
        return Mono.just(invoiceRequest)
                .flatMap(invoiceReq->{
                    Invoice invoice = INVOICE_MAPPER.invoiceRequestToInvoice(invoiceReq);
                    invoice.setDate(LocalDate.now());
                    Mono<Customer> customerMono =  customerService.getCustomerAsExchange(invoiceReq.getCustomerId())
                            .log();
                   return customerMono
                            .flatMap(customer -> {
                                invoice.setCustomerId(customer.getCustomerId());
                                invoice.setCustomer(customer);
                                return invoiceRepository.save(invoice)
                                        .map(INVOICE_MAPPER::invoiceToInvoiceResponse);
                            });

                });

    }

    @Override
    public Mono<InvoiceResponse> update(InvoiceRequest invoiceRequest, String invoiceId) {
        return invoiceRepository
                .findById(invoiceId)
                .flatMap(invoice -> {
                    Invoice invoiceSaved = INVOICE_MAPPER.invoiceRequestToInvoice(invoiceRequest);
                    invoiceSaved.setInvoiceId(invoice.getInvoiceId());

                    return invoiceRepository.save(invoiceSaved)
                            .map(INVOICE_MAPPER::invoiceToInvoiceResponse);
                })
                .switchIfEmpty(Mono.empty());
    }

    @Override
    public Mono<Void> delete(String invoiceId) {
        return invoiceRepository.deleteById(invoiceId)
                .then(Mono.empty());
    }
}
