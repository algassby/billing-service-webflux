package com.barry.billingservice.service;

import com.barry.billingservice.model.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final WebClient webClient;

    public Mono<Customer> getCustomer(String customerId){
        return webClient
                .get()
                .uri("/{customerId}", customerId)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .onStatus(HttpStatusCode::is5xxServerError, ClientResponse::createException)
                .onStatus(HttpStatusCode::is4xxClientError, clientResponse ->
                        clientResponse.bodyToMono(String.class).map(RuntimeException::new))
                .bodyToMono(Customer.class);
    }

    public Mono<Customer> getCustomerAsExchange(String customerId){
        return webClient

                .get()
                .uri("/{customerId}", customerId)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .exchangeToMono(clientResponse -> {
                    if(!clientResponse.statusCode().is2xxSuccessful()){
                        return clientResponse.createError();
                    }
                    return clientResponse.bodyToMono(Customer.class);
                });
    }
}
