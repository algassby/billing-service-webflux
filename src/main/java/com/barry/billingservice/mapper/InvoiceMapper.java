package com.barry.billingservice.mapper;

import com.barry.billingservice.dto.request.InvoiceRequest;
import com.barry.billingservice.dto.response.InvoiceResponse;
import com.barry.billingservice.model.Invoice;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface InvoiceMapper {

    InvoiceMapper INVOICE_MAPPER = Mappers.getMapper(InvoiceMapper.class);

    InvoiceResponse invoiceToInvoiceResponse(Invoice invoice);
    Invoice invoiceRequestToInvoice(InvoiceRequest invoiceRequest);
}
