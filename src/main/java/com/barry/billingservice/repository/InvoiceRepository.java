package com.barry.billingservice.repository;

import com.barry.billingservice.model.Invoice;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface InvoiceRepository extends ReactiveMongoRepository<Invoice, String> {

    Flux<Invoice> findAllByCustomerId(String customerId);
}
